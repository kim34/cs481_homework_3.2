﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class modalpage_2 : ContentPage
    {
        public modalpage_2()
        {
            InitializeComponent();
        }

        public async void BackPage_2(object sender, EventArgs e)
        {


            await Navigation.PopModalAsync();// if user click Back button, pop the page ( go to main page)

        }
    }
}