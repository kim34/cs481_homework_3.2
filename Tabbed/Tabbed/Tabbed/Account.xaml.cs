﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Account : ContentPage

    {
        modalPage modal; // connect pages between account and modalPage.
    

        public Account()
        {
            InitializeComponent();

        }
        public async void NextPage(object sender, EventArgs e)
        {
            modal = new modalPage(Entry_Name.Text);
            await Navigation.PushModalAsync(modal); // if click the Entry_Name button, then go to the modal page ( insert student ID section). 

        }


    }



}