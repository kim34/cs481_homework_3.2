﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // set navigationPage to Mainpage
            MainPage = new NavigationPage(new Tabbed.MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
