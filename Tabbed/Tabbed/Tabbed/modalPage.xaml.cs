﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class modalPage : ContentPage
    {
        public modalPage(string name)
        {
            InitializeComponent();


            if (string.IsNullOrWhiteSpace(name)) // if user do not enter name, Name will be "Your"
            {
                name = " Empty";
            }

            label_Name.Text = "Hello " + name ; // display user's Name
        }

        public async void BackPage(object sender, EventArgs e)
        {

         
            await Navigation.PopModalAsync();// if click Back button, pop the page ( go to main page)

        }

    }
}