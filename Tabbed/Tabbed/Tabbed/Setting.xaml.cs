﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Setting : ContentPage
    {

        modalpage_2 modal_2;    //connect between setting page and modalpage_2
        public Setting()
        {
            InitializeComponent();
        }

        public async void NextPage_2(object sender, EventArgs e)
        {
            modal_2 = new modalpage_2();

            await Navigation.PushModalAsync(modal_2); // if click the button, then go to the modal page ( insert student ID section). 

        }

    }
}